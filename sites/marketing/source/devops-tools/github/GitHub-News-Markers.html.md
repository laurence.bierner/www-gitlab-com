---
layout: markdown_page
title: "GitHub News Markers"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->



## GitHub Announcements
- GitHub Satellite Event: On May 6, GitHub made several feature announcements at their GitHub Satellite Event.  Here's [**GitLab's blog post**](https://about.gitlab.com/blog/2020/05/06/git-challenge/) about their event.
- GitHub Teams for Free: On April 14, 2020, GitHub announced that GitHub is now free for teams.  GitLab has been free for teams/organizations since its inception.  Read [**GitLab's blog post response and #GitChallenge**](https://about.gitlab.com/blog/2020/04/14/github-free-for-teams/) on this move by GitHub.
- 2020-04-14 - GitHub announces that it is now [Free for Teams](https://github.blog/2020-04-14-github-is-now-free-for-teams/)
- 2019-11 - GitHub announces [general availability of GitHub Actions](https://github.blog/2019-11-13-universe-day-one/) for all users.
- 2019-05-10 - GitHub announces a limited public beta for [GitHub Package Registry](https://github.com/features/package-registry).  From the website: "With GitHub Package Registry you can safely publish and consume packages within your organization or with the entire world."
- 2018-09-10 - GitHub announces that [Microsoft Azure Pipelines are available in the GitHub Marketplace](https://blog.github.com/2018-09-10-azure-pipelines-now-available-in-github-marketplace/). This is in parallel with Microsoft (who recently acquired GitHub) announcing their Azure DevOps (rebranded VSTS/TFS). Related? Yes!
- 2018-09-10 - Microsoft announces the [addition of GitHub Pull Requests integrated directly into Visual Studio Code](https://code.visualstudio.com/blogs/2018/09/10/introducing-github-pullrequests). Visual Studio Code is [Microsoft's lightweight (but still not web based) code editor](https://azure.microsoft.com/en-us/products/visual-studio-code/).
- 2018-06-04 - Microsoft acquires GitHub.