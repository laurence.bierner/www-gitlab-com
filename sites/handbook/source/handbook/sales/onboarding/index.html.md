---
layout: handbook-page-toc
title: "Sales & Customer Success Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## The Goals of Sales & Customer Success Onboarding
Sales onboarding at GitLab is a blended learning experience (virtual, self-paced learning path paired with an immersive, hands-on in-person workshop called Sales Quick Start) focused on what new sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** within their first 30 days or so on the job so they are "customer conversation ready" and they are comfortable, competent, and confident leading customer engagements before the end of their first several weeks on the job. There are some formal learning components with a heavy emphasis on social learning (learning from others) and learning by doing to help bridge the knowing-going gap. 

For a detailed list of SQS Learning Objectives, please visit [this page](https://about.gitlab.com/handbook/sales/onboarding/sqs-learning-objectives/).

## Key SQS Details

*  [Sales & Customer Success Quick Start Learning Path](https://about.gitlab.com/handbook/sales/onboarding/sales-learning-path/)

## Upcoming Sales Quick Start Workshops

| DATES | GEO | LOCATION | ISSUE |
| ------ | ------ | ------ | ------ |
| Sep 28-Oct 7, 2020 | AMER | Virtual | [issue](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/471) | 

## Graduating from Sales Onboarding
In order to officially “graduate” from Sales Onboarding at GitLab, we have identified a few components that are considered milestones of achievement (many of these must be done concurrently to ensure completion):

Sales Roles:
*  Complete GitLab General Onboarding issue
*  Complete Sales Quick Start learning path in Google Classroom prior to the SQS Workshop
*  Complete the Sales Quick Start Workshop
*  Deliver GitLab Value Pitch
*  Articulate the Command of the Message Mantra
*  Complete a series of discovery calls:
   - Submit 1 recorded mock discovery call in SQS pre-work
   - Complete 1 mock discovery call at the SQS Workshop
   - Submit 1 “Live Lead” after the SQS Workshop within 30 days
*  Review and obtain approval from your manager for a territory and account plan
*  Close first deal within the first 90 days of starting at GitLab

Customer Success Roles: 
*  Complete GitLab General Onboarding issue
*  Complete Sales Quick Start learning path in Google Classroom prior to the SQS Workshop
*  Complete the Sales Quick Start Workshop
*  Deliver the role based Capstone 
   * TAM: Mock customer kickoff
   * SA: Build and deliver mock demo
   * PSE: Statement of Work
*  Articulate the Command of the Message Mantra
*  Kickoff first customer engagement within the first 90 days of starting at GitLab


## Sales Quick Start Workshop
*  NOTE: The Sales Quick Start (SQS) Workshop is MANDATORY for all new Sales Team Members. If you must cancel for any reason, please be sure to obtain Regional Director approval in writing (Ryan O'Nell for Commercial).
*  After completing the virtual, self-paced Sales Quick Start learning path, new sales team members (along with new Solution Architects, Technical Account Managers, and Sales Development Rep) participate in an interactive in-person workshop where participants practice applying this new knowledge in fun and challenging ways including mock calls, role plays, and group activities
   - As a result, new sales team members exit Sales Quick Start more confident and capable in their ability to lead an effective discovery call with a customer or prospect
   - Participants also benefit by establishing a community of similarly-tenured peers and more experienced mentors and colleagues to support each others' growth and development at GitLab
*  See what's included in the [Sales Quick Start in-person workshop](/handbook/sales/onboarding/#sales-quick-start-in-person-workshop-agenda)
*  **Note: The Results Value is our highest priority and we strive to deliver sales training and enablement remotely as much as possible and reserve in-person formats only for circumstances when we can't obtain the desired results via remote/virtual delivery)**
   - Iteration is expected to keep moving what we can to remote models once we can achieve the same or better results that way
*  Future iteration of this process will define what sales team members need to **KNOW**, **DO**, and **BE ABLE TO ARTICULATE** (by sales role, customer segment, and geo as appropriate) after they complete the above and support those learning objectives as they continue their ramp to GitLab sales performance excellence!

## Swag for New Sales Team Members

As a part of Sales Onboarding, each new Americas Sales team member is allowed to order one Swag Marketing Kit through this [form](https://docs.google.com/forms/d/e/1FAIpQLSflKtSu5xyYERATBHGswwMjn4NsUc8DMTxfKQXDAZ0FqdEYCg/viewform). As of right now, there is no Swag Marketing Kit available for our other regions, but the Marketing team is working towards developing one. 

## Sales & Customer Success Quick Start Pre-Work Learning Path 

* [Quick Start Learning Path for both Sales and Customer Success Teams](/handbook/sales/onboarding/sales-learning-path/)
