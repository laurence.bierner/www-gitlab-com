---
layout: handbook-page-toc
title: Communication tips
category: References 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Overview

This page is intended to provide general communication tips for GitLab Support
Department. Anything related to communication useful for this department is
welcome here. It is meant to be the extension of [GitLab Communication handbook page](/handbook/communication/).

*Examples:*
 1. *How can Support Engineer efficiently talk to other Support Engineers via Slack, Zoom or e-mail?*
 2. *How can Support Engineer get most of their interaction with Technical Account Manager?*

---

##### Slack reactions

In addition to `:white_check_mark:` emoji that is mentioned in [General Guidelines for Communication at GitLab](/handbook/communication/#general-guidelines), consider utilizing the following emojis within Support Slack channels as well:

1. `:idontknow:` - when another Support Engineer is asking a question which you don't know the answer to
1. `:red_circle:` - when another Support Engineer is asking for others to take a look at something or similar, but you are unable to do that within reasonable time (Example is when another engineer is asking for someone to replace them for their on-call shift, but you are unavailable at the time they asked for. Another example is when [SLAH](/handbook/support/workflows/meeting-service-level-objectives.html#what-is-the-sla-hawk-role) is asking whether someone can respond to ticket breaching in one hour, but you have no bandwidth to do that within the SLA.)
1. `:escalation:` - when a member of another department requests that an existing Zendesk ticket or issue in `internal-requests` be escalated or looked into in either the #support_gitlab-com or #support_self-managed channels. This will redirect them to post in #support_managers instead.
1. `:leftwards_arrow_with_hook:` - when a member of another department posts a general question about GitLab or GitLab.com in the #support_gitlab-com channel. This will redirect them to post in #questions or #is-this-known instead.

Note that it is better to send some signal (even though it might mean you cannot help) than to ignore the message that has been sent. This helps the person who started the thread as it avoids the situation where they still hope someone will chime in and makes them focus on searching for help elsewhere. Of course, whenever you can, share the idea you have in your mind no matter how trivial you consider it to be, as you never know what can lead someone else in the right direction.

---
##### Tickets

Whenever there is a discussion in Slack regarding a ticket, please remember to 
add a link to the discussion as an internal note on the ticket. This will allow 
anyone working on the ticket after you to easily find the discussion.

---
##### `@mustread`
The [@must-read Slack bot](https://finalem.com/must-read) transforms any important message into micro-task. So, no one will miss it. It is a bot that will helps our team to collect important messages, announcements and links, and track who have read it.
@must-read helps to control all significant information no matter how many channels you have and how big is your team. You can easily check who has read the message and who has not. No more “Did you read it guys?” questions needed.

###### Use cases
- Use it to send short important announcements for whole team or channel
- Use it when your message is too small to become a task/card/issue but too important to be forgotten/lost/ignored
- Bookmark important links for yourself or any member of your team
- Send links to important news, tasks, meetings or support tickets
- Use must-read messages as simple reminders
- When you got notifications from Zendesk, Trello, Asana, JIRA, etc. that need more attention, simply        start a thread and mention `@must-read`

###### Workflow
1. Here are some simple commands that you can
   - Mark any messages to track reactions of the team:
      - Mention `@mustread` and teammates (who must read) if you want to get their reaction.
        Also you can use @channel or @here. I'll collect all these messages in follow-up list.
        E.g. `@must-read: example.net @user1 @user2`
        I'll add ✅ to this message. If somebody clicks on this reaction, it will mean they read it. When everyone has reacted you will see 📙.

   - Stealth mode:
      - If you add nobody but `@mustread` to any link (or 70+ chars of important text for your teammates) it will be must-read for everyone in the channel.
        E.g. `@must-read: example.net`
        or `@must-read: some important text for a channel 70 or more characters long`

Please refer to the [help page](https://finalem.com/must-read/help) for more information on different use cases.
